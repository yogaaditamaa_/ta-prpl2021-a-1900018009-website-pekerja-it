<?php

    date_default_timezone_set("Asia/Jakarta");

    $queryCekPelamar    = "SELECT nama_pelamar, jenis_kelamin, cv FROM pelamar WHERE id_pelamar='$_GET[id_pelamar]'";
    $prosesCekPelamar   = mysqli_query($conn, $queryCekPelamar);
    $resultCekPelamar   = mysqli_fetch_assoc($prosesCekPelamar);

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-file-signature"></i> Cek Pelamar</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" class="col-12">
                    <div class="p-5">

                        <div class="form-group">
                            <label>Nama Pelamar</label>
                            <input type="text" class="form-control form-control-user" value="<?php echo $resultCekPelamar['nama_pelamar']; ?>" readonly>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <input type="text" class="form-control form-control-user" value="<?php echo $resultCekPelamar['jenis_kelamin']; ?>" readonly>
                        </div>
                        <div class="form-group text-center mt-4">
                            <h2>CV</h2>
                            <br>
                            <img src="../assets/img/cv/<?= $resultCekPelamar['cv']; ?>" alt="<?= $resultCekPelamar['nama_pelamar']; ?>" class="img-fluid">
                        </div>

                        <a href="index.php?content=lowongan" class="btn btn-lg btn-warning btn-user btn-block"><i class="fas fa-caret-left"></i> KEMBALI</a>

                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
<?php

    date_default_timezone_set("Asia/Jakarta");

    if (isset($_POST['selesai'])) {
        $id_perusahaan          = $_POST['id_perusahaan'];
        $id_kategori_lowongan   = $_POST['id_kategori_lowongan'];
        $nama_lowongan          = $_POST['nama_lowongan'];
        $gaji                   = $_POST['gaji'];
        $deskripsi              = $_POST['deskripsi'];
        $status                 = "Active";
        $date                   = date("l, d F Y");

        // Include gambar
        $gambar                 = $_FILES['gambar']['name']; // mendapatkan nama gambar
        $lokasi_gambar          = $_FILES['gambar']['tmp_name']; // mendapatkan lokasi gambar
        $tipe_file_gambar       = format_gambar(strtolower($_FILES['gambar']['type']));
        $tujuan_gambar          = '../assets/img/lowongan'; // pindah gambar tersebut ke lokasi ini
        $nama_gambar_unik       = "gambar-".karakter_unik($nama_lowongan)."-".$id_perusahaan.".".$tipe_file_gambar;
        $upload_gambar          = move_uploaded_file($lokasi_gambar, $tujuan_gambar.'/'.$nama_gambar_unik); // function mengupload/memindahkan file ke direktori yang di maksud

        $queryBuatLowongan      = "INSERT INTO lowongan VALUES ('', '$id_perusahaan', '$id_kategori_lowongan', '$nama_lowongan', '$gaji', '$nama_gambar_unik', '$deskripsi', '$status', '$date')";
        $prosesBuatLowongan     = mysqli_query($conn, $queryBuatLowongan);

        if (!empty($prosesBuatLowongan)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=lowongan';</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-edit"></i> Buat Lowongan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" class="col-12" enctype="multipart/form-data">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-primary mb-4">Silahkan isi data di bawah ini dengan lengkap & benar!</h1>
                        </div>

                        <input type="hidden" class="form-control form-control-user" name="id_perusahaan" value="<?php echo $_SESSION['id_perusahaan']; ?>" required>

                        <div class="form-group">
                            <label for="id_kategori_lowongan">Kategori Lowongan</label>
                            <select id="id_kategori_lowongan" name="id_kategori_lowongan" class="custom-select">
                                <?php

                                    $queryKategori  = "SELECT * FROM kategori_lowongan";
                                    $prosesKategori = mysqli_query($conn, $queryKategori);
                                    while ($resultKategori   = mysqli_fetch_assoc($prosesKategori)) {

                                ?>
                                <option value="<?php echo $resultKategori['id_kategori_lowongan']; ?>"><?php echo $resultKategori['nama_kategori_lowongan']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="nama_lowongan">Nama Lowongan</label>
                            <input type="text" id="nama_lowongan" class="form-control form-control-user" name="nama_lowongan" placeholder="Masukkan Nama Lowongan" required>
                        </div>
                        <div class="form-group">
                            <label for="gaji">Gaji</label>
                            <input type="number" id="gaji" class="form-control form-control-user" name="gaji" placeholder="Masukkan Gaji" required>
                        </div>
                        <div class="form-group">
                            <label for="gambar">Pilih Gambar</label>
                            <input type="file" id="gambar" class="form-control-file form-control-user" name="gambar" required>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Lowongan</label>
                            <textarea id="deskripsi" class="ckeditor" name="deskripsi" required></textarea>
                        </div>
                        <button type="submit" name="selesai" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
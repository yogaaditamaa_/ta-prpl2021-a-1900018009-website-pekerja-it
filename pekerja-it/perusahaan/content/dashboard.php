<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard Web Pekerja IT</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <?php

            $queryPelamar       = "SELECT COUNT(daftar_lamaran.id_daftar_lamaran) AS jumlahPelamar FROM daftar_lamaran INNER JOIN lowongan ON daftar_lamaran.id_lowongan = lowongan.id_lowongan WHERE lowongan.id_perusahaan='$_SESSION[id_perusahaan]' AND lowongan.status='Active' ";
            $prosesPelamar      = mysqli_query($conn, $queryPelamar);
            $resultPelamar      = mysqli_fetch_assoc($prosesPelamar);

        ?>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Pelamar Anda</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rupiah($resultPelamar['jumlahPelamar']); ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-pray fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
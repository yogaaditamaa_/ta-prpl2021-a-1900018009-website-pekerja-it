<?php

    date_default_timezone_set("Asia/Jakarta");

    $queryCekPelamar    = "SELECT nama_pelamar, jenis_kelamin, cv FROM pelamar WHERE id_pelamar='$_GET[id_pelamar]'";
    $prosesCekPelamar   = mysqli_query($conn, $queryCekPelamar);
    $resultCekPelamar   = mysqli_fetch_assoc($prosesCekPelamar);

    if (isset($_POST['acc_pelamar'])) {
        $id_daftar_lamaran      = $_GET['id_daftar_lamaran'];
        $id_lowongan            = $_GET['id_lowongan'];
        $fee_royalty_perusahaan = $_POST['fee_royalty_perusahaan'];

        $queryFeeRoyalty   = "INSERT INTO fee_royalty VALUES ('', '$id_daftar_lamaran', '$fee_royalty_perusahaan')";
        $prosesFeeRoyalty  = mysqli_query($conn, $queryFeeRoyalty);

        if (!empty($prosesFeeRoyalty)) {

            $queryDaftarLamaran   = "UPDATE daftar_lamaran SET status='ACC' WHERE id_daftar_lamaran='$id_daftar_lamaran'";
            $prosesDaftarLamaran  = mysqli_query($conn, $queryDaftarLamaran);

            if (!empty($prosesDaftarLamaran)) {

                $queryEditLowongan   = "UPDATE lowongan SET status='Off' WHERE id_lowongan='$id_lowongan'";
                $prosesEditLowongan  = mysqli_query($conn, $queryEditLowongan);

                if (!empty($prosesEditLowongan)) {
                    echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=pelamar';</script>";
                }else{
                    echo "<script>window.alert('Gagal edit lowongan!'); window.location(history.back(-1))</script>";
                }

            }else{
                echo "<script>window.alert('Gagal edit daftar lamaran!'); window.location(history.back(-1))</script>";
            }

        }else{
            echo "<script>window.alert('Gagal input fee royalty!'); window.location(history.back(-1))</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-check-double"></i> ACC Pelamar</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" class="col-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-primary mb-4">Silahkan isi data di bawah ini dengan lengkap & benar!</h1>
                        </div>

                        <button class="btn btn-block btn-flat rounded-0 btn-warning mb-4" type="button" data-toggle="collapse" data-target="#cekDataPelamar" aria-expanded="false" aria-controls="cekDataPelamar">
                            Cek Data Pelamar <i class="fas fa-search"></i>
                        </button>
                        <div class="collapse mb-4" id="cekDataPelamar">
                            <div class="card card-body">
                                <div class="form-group">
                                    <label>Nama Pelamar</label>
                                    <input type="text" class="form-control form-control-user" value="<?php echo $resultCekPelamar['nama_pelamar']; ?>" readonly>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <input type="text" class="form-control form-control-user" value="<?php echo $resultCekPelamar['jenis_kelamin']; ?>" readonly>
                                </div>
                                <div class="form-group text-center mt-4">
                                    <h2>CV</h2>
                                    <br>
                                    <img src="../assets/img/cv/<?= $resultCekPelamar['cv']; ?>" alt="<?= $resultCekPelamar['nama_pelamar']; ?>" class="img-fluid">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="fee_royalty_perusahaan">Fee Royalty</label>
                            <input type="number" id="fee_royalty_perusahaan" class="form-control form-control-user" name="fee_royalty_perusahaan" placeholder="Mohon isi Fee Royalty" required>
                        </div>
                        <button type="submit" name="acc_pelamar" class="btn btn-lg btn-primary btn-user btn-block"><i class="fas fa-check-double"></i> ACC PELAMAR</button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
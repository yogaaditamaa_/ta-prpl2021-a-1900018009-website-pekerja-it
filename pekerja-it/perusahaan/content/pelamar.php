<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-pray fa-2x text-gray-300"></i> Daftar Pelamar</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pelamar</th>
                                <th>Lowongan</th>
                                <th>Cek CV</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryDaftarPelamar  = "SELECT daftar_lamaran.id_daftar_lamaran, pelamar.nama_pelamar, pelamar.id_pelamar, pelamar.cv, lowongan.nama_lowongan, lowongan.id_lowongan FROM daftar_lamaran INNER JOIN pelamar ON daftar_lamaran.id_pelamar = pelamar.id_pelamar INNER JOIN lowongan ON daftar_lamaran.id_lowongan = lowongan.id_lowongan WHERE lowongan.id_perusahaan='$_SESSION[id_perusahaan]' AND lowongan.status='Active' ORDER BY id_daftar_lamaran DESC";
                                $prosesDaftarPelamar = mysqli_query($conn, $queryDaftarPelamar);
                                while ($resultDaftarPelamar   = mysqli_fetch_assoc($prosesDaftarPelamar)) {

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $resultDaftarPelamar['nama_pelamar']; ?></td>
                                <td><h5 class="text-primary"><?= $resultDaftarPelamar['nama_lowongan']; ?></h5></td>
                                <td class="text-center">
                                    <a target="_blank" href="../assets/img/cv/<?php echo $resultDaftarPelamar['cv']; ?>" class="btn btn-warning">
                                        <i class="fas fa-external-link-alt"></i> CV
                                    </a>
                                </td>
                                <td class="text-center">
                                    <a href="index.php?content=edit-pelamar&id_pelamar=<?= $resultDaftarPelamar['id_pelamar']; ?>&id_daftar_lamaran=<?= $resultDaftarPelamar['id_daftar_lamaran']; ?>&id_lowongan=<?= $resultDaftarPelamar['id_lowongan']; ?>" class="btn btn-info">
                                        <i class="fas fa-check-double"></i> ACC
                                    </a>
                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
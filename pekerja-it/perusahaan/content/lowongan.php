<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-briefcase"></i> Lowongan</h1>
        <a href="index.php?content=buat-lowongan" role="button" class="btn btn-primary"><i class="far fa-edit"></i> Buat Lowongan</a>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Nama Lowongan</th>
                                <th>Gaji</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryLowongan  = "SELECT lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.gaji, lowongan.status, kategori_lowongan.nama_kategori_lowongan FROM lowongan INNER JOIN kategori_lowongan ON lowongan.id_kategori_lowongan = kategori_lowongan.id_kategori_lowongan WHERE id_perusahaan='$_SESSION[id_perusahaan]'";
                                $prosesLowongan = mysqli_query($conn, $queryLowongan);
                                while ($resultLowongan   = mysqli_fetch_assoc($prosesLowongan)) {

                                    $queryCekIdPelamar  = "SELECT id_pelamar FROM daftar_lamaran WHERE id_lowongan='$resultLowongan[id_lowongan]' AND status='ACC'";
                                    $prosesCekIdPelamar = mysqli_query($conn, $queryCekIdPelamar);
                                    $resultCekIdPelamar = mysqli_fetch_assoc($prosesCekIdPelamar);

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><button type="button" class="btn btn-primary"><?= $resultLowongan['nama_kategori_lowongan']; ?></button></td>
                                <td><?= $resultLowongan['nama_lowongan']; ?></td>
                                <td>Rp<?= rupiah($resultLowongan['gaji']); ?></td>
                                <td>
                                    <?php if ($resultLowongan['status']=="Active"): ?>
                                        <button type="button" class="btn btn-block btn-success"><i class="fas fa-check-double"></i> Active</button>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-danger"><i class="far fa-times-circle"></i> Off</button>
                                        <a href="index.php?content=cek-pelamar&id_pelamar=<?php echo $resultCekIdPelamar['id_pelamar']; ?>" class="btn btn-warning">
                                            <i class="fas fa-file-signature"></i> Cek
                                        </a>
                                    <?php endif ?>
                                </td>
                                <td class="text-center">
                                    <a href="index.php?content=edit-lowongan&id=<?= $resultLowongan['id_lowongan']; ?>" class="btn btn-info">
                                        <i class="fas fa-edit"></i> Edit
                                    </a>
                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
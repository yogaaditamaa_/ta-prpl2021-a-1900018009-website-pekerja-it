<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?content=dashboard">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin <sup>Web IT</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item <?php if($_GET['content']=='dashboard'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu Utama
    </div>

    <li class="nav-item <?php if($_GET['content']=='lowongan' OR $_GET['content']=='buat-lowongan' OR $_GET['content']=='edit-lowongan' OR $_GET['content']=='cek-pelamar'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=lowongan">
            <i class="fas fa-briefcase"></i>
            <span>Lowongan Saya</span></a>
    </li>

    <li class="nav-item <?php if($_GET['content']=='pelamar' OR $_GET['content']=='edit-pelamar'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=pelamar">
            <i class="fas fa-pray fa-2x text-gray-300"></i>
            <span>Daftar Pelamar</span></a>
    </li>
    
    <!-- <li class="nav-item <?php if($_GET['content']=='pelamar' OR $_GET['content']=='edit-pelamar'){ echo 'active'; } ?>">
        <a class="nav-link" href="index.php?content=pelamar">
            <i class="fas fa-pray fa-2x text-gray-300"></i>
            <span>Daftar Pelamar</span></a>
    </li> -->

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
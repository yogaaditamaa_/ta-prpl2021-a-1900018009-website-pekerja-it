Website ini memudahkan perusahaan yang sedang mencari pekerja IT untuk menemukan programmer yang sesuai dan dibutuhkan di persuhaan mereka. Fitur nya

Pelamar :
1. Mendaftarkan diri
2. Memasukkan data diri beserta CV
3. Membayar Jasa menaruh CV di website

Pencari :
1. Mendaftarkan Diri
2. Dapat melihat CV dari para pelamar
3. Memilih pelamar yang sesuai
4. Membayar jasa agar dipertemukan dengan Pelamar

Halm Transaksi :
1. Biaya jasa


============ DATABASE ============

Admin
	- id_admin
	- username
	- password

Pelamar
	- id_pelamar
	- nama_pelamar
	- jenis_kelamin
	- cv
	- username
	- password
	- status (On/Off)

Perusahaan
	- id_perusahaan
	- nama_perusahaan
	- lokasi_perusahaan
	- username
	- password

Kategori Lowongan
	- id_kategori_lowongan
	- nama_kategori_lowongan

Lowongan
	- id_lowongan
	- id_perusahaan
	- id_kategori_lowongan
	- nama_lowongan
	- gaji
	- gambar_lowongan
	- deskripsi
	- status (Active/Off)
	- date

Daftar Lamaran
	- id_daftar_lamaran
	- id_pelamar
	- id_lowongan
	- status (Pending/ACC)

Fee Royalty
	- id_fee_royalty
	- id_daftar_lamaran
	- fee_royalty_pelamar
	- fee_royalty_perusahaan

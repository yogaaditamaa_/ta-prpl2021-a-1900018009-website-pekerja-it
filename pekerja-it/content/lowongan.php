<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
        <div class="container text-center">
            <form action="" method="POST">
                <div class="form-group">
                    <input type="text" class="form-control" name="querycari" placeholder="Pekerjaan apa yang anda cari?" required>
                </div>
                <button type="submit" name="cari" class="btn btn-primary">Cari Pekerjaan <i class="icofont-search-document"></i></button>
            </form>
        </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Popular Courses Section ======= -->
    <section id="popular-courses" class="courses">
        <div class="container">

            <?php if (isset($_POST['cari'])): ?>
                <div class="section-title">
                    <h2>Yang anda cari: </h2>
                    <p><?php echo $_POST['querycari']; ?></p>
                </div>
            <?php else: ?>
                <div class="section-title">
                    <h2>Daftar</h2>
                    <p>Lowongan Pekerjaan</p>
                </div>
            <?php endif ?>

            <div class="row">

                <?php

                    $no = 1;

                    if (isset($_POST['cari'])) {
                        $querycari      = $_POST['querycari'];
                        $queryLowongan  = "SELECT
                                        lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.gaji, lowongan.gambar_lowongan, lowongan.deskripsi,
                                        kategori_lowongan.nama_kategori_lowongan,
                                        perusahaan.nama_perusahaan
                                        FROM lowongan
                                        INNER JOIN kategori_lowongan ON lowongan.id_kategori_lowongan = kategori_lowongan.id_kategori_lowongan
                                        INNER JOIN perusahaan ON lowongan.id_perusahaan = perusahaan.id_perusahaan
                                        WHERE lowongan.status='Active'
                                        AND nama_lowongan LIKE '%$querycari%'";
                    }else{
                        $queryLowongan  = "SELECT
                                        lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.gaji, lowongan.gambar_lowongan, lowongan.deskripsi,
                                        kategori_lowongan.nama_kategori_lowongan,
                                        perusahaan.nama_perusahaan
                                        FROM lowongan
                                        INNER JOIN kategori_lowongan ON lowongan.id_kategori_lowongan = kategori_lowongan.id_kategori_lowongan
                                        INNER JOIN perusahaan ON lowongan.id_perusahaan = perusahaan.id_perusahaan
                                        WHERE lowongan.status='Active'
                                        ORDER BY id_lowongan DESC";
                    }

                    
                    $prosesLowongan = mysqli_query($conn, $queryLowongan);
                    while ($resultLowongan   = mysqli_fetch_assoc($prosesLowongan)) {

                        $queryJumlahPelamar  = "SELECT COUNT(id_daftar_lamaran) AS jumlahPelamar FROM daftar_lamaran WHERE id_lowongan='$resultLowongan[id_lowongan]'";
                        $prosesJumlahPelamar = mysqli_query($conn, $queryJumlahPelamar);
                        $resultJumlahPelamar = mysqli_fetch_assoc($prosesJumlahPelamar);

                ?>

                <div class="col-lg-4 col-md-6 mb-4 d-flex align-items-stretch">
                    <div class="course-item">
                        <img src="assets/img/lowongan/<?php echo $resultLowongan['gambar_lowongan']; ?>" class="img-fluid" style="height: 250px;width: 100%;" alt="<?php echo $resultLowongan['nama_lowongan']; ?>">
                        <div class="course-content">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4><?php echo $resultLowongan['nama_kategori_lowongan']; ?></h4>
                                <p class="price">Rp<?php echo rupiah($resultLowongan['gaji']); ?></p>
                            </div>

                            <h3><a href="index.php?content=detail-lowongan&id=<?php echo $resultLowongan['id_lowongan']; ?>"><?php echo $resultLowongan['nama_lowongan']; ?></a></h3>
                            <p><?php echo strip_tags(substr($resultLowongan['deskripsi'], 0, 100)); ?> ...</p>

                            <div class="trainer d-flex justify-content-between align-items-center">
                                <div class="trainer-profile d-flex align-items-center">
                                    <i class="icofont-building-alt"></i>
                                    <span><?php echo $resultLowongan['nama_perusahaan']; ?></span>
                                </div>
                                <div class="trainer-rank d-flex align-items-center">
                                    <i class="icofont-users-alt-5"></i>&nbsp;<?php echo $resultJumlahPelamar['jumlahPelamar']; ?></span>&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Course Item-->

                <?php } ?>

            </div>

        </div>
    </section><!-- End Popular Courses Section -->

</main><!-- End #main -->
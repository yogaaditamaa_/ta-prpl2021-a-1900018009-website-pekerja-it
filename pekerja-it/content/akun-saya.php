<?php

    if (empty($_SESSION['id_pelamar']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        header('location: login.php');
    }

?>

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
        <div class="container">
            <h2>Hay, <?php echo $_SESSION['nama_pelamar']; ?></h2>
            <a href="logout.php" role="button" class="btn btn-primary">LOGOUT <i class="icofont-logout"></i></a>
        </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Events Section ======= -->
    <section id="events" class="events">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-img">
                            <?php if ($_SESSION['jenis_kelamin']=="Laki-Laki"): ?>
                                <img src="assets/img/laki-laki.jpg" class="img-fluid" alt="<?php echo $_SESSION['nama_pelamar']; ?>">
                            <?php else: ?>
                                <img src="assets/img/perempuan.jpg" class="img-fluid" alt="<?php echo $_SESSION['nama_pelamar']; ?>">
                            <?php endif ?>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href=""><?php echo $_SESSION['nama_pelamar']; ?></a></h5>
                            <p class="font-italic text-center"><?php echo $_SESSION['jenis_kelamin']; ?></p>
                            <h5 class="card-title mt-4 pt-4">CV SAYA</h5>
                            <img src="assets/img/cv/<?php echo $_SESSION['cv']; ?>" class="img-fluid" alt="<?php echo $_SESSION['cv']; ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                    <h4 style="color: #5FCF80;font-weight: bold;"><i class="icofont-file-document"></i> DAFTAR LAMARAN SAYA</h4>

                    <table class="table table-bordered table-striped mt-4">
                        <thead class="text-light" style="background-color: #5FCF80;">
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th style="width: 50%;">Lamaran</th>
                                <th style="width: 15%;">Link</th>
                                <th style="width: 30%;">Status</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php

                                $no = 1;
                                $queryDaftarLamaran     = "SELECT daftar_lamaran.id_daftar_lamaran, daftar_lamaran.status, lowongan.id_lowongan, lowongan.nama_lowongan FROM daftar_lamaran INNER JOIN lowongan ON daftar_lamaran.id_lowongan = lowongan.id_lowongan WHERE id_pelamar='$_SESSION[id_pelamar]' ORDER BY id_daftar_lamaran DESC";
                                $prosesDaftarLamaran    = mysqli_query($conn, $queryDaftarLamaran);
                                while ($resultDaftarLamaran   = mysqli_fetch_assoc($prosesDaftarLamaran)) {

                            ?>
                            <tr>
                                <th><?php echo $no++; ?></th>
                                <td><?php echo $resultDaftarLamaran['nama_lowongan']; ?></td>
                                <td><a href="index.php?content=detail-lowongan&id=<?php echo $resultDaftarLamaran['id_lowongan']; ?>" role="button" class="btn btn-primary"><i class="icofont-external-link"></i></a></td>
                                <td>
                                    <?php if ($resultDaftarLamaran['status']=='Pending'): ?>
                                        <button type="button" class="btn btn-danger">PENDING</button>
                                    <?php else: ?>
                                        <button type="button" class="btn btn-success">ACC</button>
                                    <?php endif ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>

        </div>
    </section><!-- End Events Section -->

</main><!-- End #main -->
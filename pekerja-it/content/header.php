<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <h1 class="logo mr-auto"><a href="index.php?content=home">Pekerja IT</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.php" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav class="nav-menu d-none d-lg-block">
            <ul>
                <li class="<?php if ($_GET['content']=='home') { echo 'active'; } ?>"><a href="index.php?content=home">Home</a></li>
                <li class="<?php if ($_GET['content']=='lowongan' OR $_GET['content']=='cari-lowongan' OR $_GET['content']=='detail-lowongan') { echo 'active'; } ?>"><a href="index.php?content=lowongan">Lowongan</a></li>
            </ul>
        </nav><!-- .nav-menu -->

        <a href="index.php?content=akun-saya" class="get-started-btn"><i class="icofont-user-alt-5"></i> Akun Saya</a>

    </div>
</header>
<footer id="footer">

    <div class="container d-md-flex py-4">

        <div class="mr-md-auto text-center text-md-left">
            <div class="copyright">
                &copy; Copyright <strong><span>Web Pekerja IT</span></strong>. All Rights Reserved
            </div>
        </div>
    </div>
</footer>
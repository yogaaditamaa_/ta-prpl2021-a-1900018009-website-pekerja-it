<?php 

    if (empty($_SESSION['id_pelamar']) AND empty($_SESSION['username']) AND empty($_SESSION['password'])) {
        $id_pelamarNya     = 0;
    }else{
        $id_pelamarNya     = $_SESSION['id_pelamar'];
    }

    $queryDetail    = "SELECT
                    lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.gaji, lowongan.gambar_lowongan, lowongan.deskripsi, lowongan.waktu,
                    kategori_lowongan.nama_kategori_lowongan
                    FROM lowongan
                    INNER JOIN kategori_lowongan ON lowongan.id_kategori_lowongan = kategori_lowongan.id_kategori_lowongan
                    WHERE lowongan.id_lowongan='$_GET[id]'";
    $prosesDetail   = mysqli_query($conn, $queryDetail);
    $resultDetail   = mysqli_fetch_assoc($prosesDetail);

?>

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <div class="breadcrumbs" data-aos="fade-in">
        <div class="container">
            <h2><?php echo $resultDetail['nama_lowongan']; ?></h2>
        </div>
    </div><!-- End Breadcrumbs -->

    <!-- ======= Cource Details Section ======= -->
    <section id="course-details" class="course-details">
        <div class="container" data-aos="fade-up">

            <div class="row">
                <div class="col-lg-8">
                    <img src="assets/img/lowongan/<?php echo $resultDetail['gambar_lowongan']; ?>" class="img-fluid" alt="<?php echo $resultDetail['nama_lowongan']; ?>">
                    <h3><?php echo $resultDetail['nama_lowongan']; ?></h3>
                    <p><?php echo $resultDetail['deskripsi']; ?>
                    </p>
                </div>
                <div class="col-lg-4">

                    <a onclick="return confirm('Apakah anda yakin ingin melamar pekerjaan ini?')" href="content/proses-lamar-pekerjaan.php?id_pelamar=<?php echo $id_pelamarNya; ?>&id_lowongan=<?php echo $resultDetail['id_lowongan']; ?>" role="button" class="btn btn-lg btn-block rounded-0 shadow mb-4" style="background-color: #5ECD7F;color: #fff;"><i class="icofont-paper-plane"></i> LAMAR</a>

                    <div class="course-info d-flex justify-content-between align-items-center mt-4">
                        <h5></h5>
                        <p style="color: #5FCE80;"><?php echo $resultDetail['nama_lowongan']; ?></p>
                    </div>

                    <div class="course-info d-flex justify-content-between align-items-center">
                        <h5>Kategori</h5>
                        <p><a href="#"><?php echo $resultDetail['nama_kategori_lowongan']; ?></a></p>
                    </div>

                    <div class="course-info d-flex justify-content-between align-items-center">
                        <h5>Gaji</h5>
                        <p>Rp<?php echo rupiah($resultDetail['gaji']); ?></p>
                    </div>

                    <div class="course-info d-flex justify-content-between align-items-center">
                        <h5></h5>
                        <p><i class="icofont-ui-calendar"></i> <?php echo $resultDetail['waktu']; ?></p>
                    </div>

                </div>
            </div>

        </div>
    </section><!-- End Cource Details Section -->

</main><!-- End #main -->
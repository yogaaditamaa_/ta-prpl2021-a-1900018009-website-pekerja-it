<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex justify-content-center align-items-center">
    <div class="container position-relative">
        <h1>Temukan pekerjaan,<br>Terbaik untuk Anda!</h1>
        <h2>Kami memiliki banyak referensi pekerjaan bidang IT untuk anda</h2>
        <a href="index.php?content=lowongan" class="btn-get-started">Cari Pekerjaan</a>
    </div>
</section>
<!-- End Hero -->

<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container">

            <div class="section-title">
                <h2>About</h2>
                <p>About Us</p>
            </div>

            <div class="row">
                <div class="col-lg-6 order-1 order-lg-2">
                    <img src="assets/img/about.jpg" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content">
                    <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>
                    <p class="font-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <ul>
                        <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                        <li><i class="icofont-check-circled"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                        <li><i class="icofont-check-circled"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
                    </ul>
                    <p>
                        Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                    </p>
                </div>
            </div>

        </div>
    </section>
    <!-- End About Section -->

    <!-- ======= Popular Courses Section ======= -->
    <section id="popular-courses" class="courses">
        <div class="container">

            <div class="section-title">
                <h2>Daftar</h2>
                <p>Lowongan Terbaru</p>
            </div>

            <div class="row">

                <?php

                    $no = 1;
                    $queryLowongan  = "SELECT
                                        lowongan.id_lowongan, lowongan.nama_lowongan, lowongan.gaji, lowongan.gambar_lowongan, lowongan.deskripsi,
                                        kategori_lowongan.nama_kategori_lowongan,
                                        perusahaan.nama_perusahaan
                                        FROM lowongan
                                        INNER JOIN kategori_lowongan ON lowongan.id_kategori_lowongan = kategori_lowongan.id_kategori_lowongan
                                        INNER JOIN perusahaan ON lowongan.id_perusahaan = perusahaan.id_perusahaan
                                        WHERE lowongan.status='Active'
                                        ORDER BY id_lowongan DESC LIMIT 3";
                    $prosesLowongan = mysqli_query($conn, $queryLowongan);
                    while ($resultLowongan   = mysqli_fetch_assoc($prosesLowongan)) {

                        $queryJumlahPelamar  = "SELECT COUNT(id_daftar_lamaran) AS jumlahPelamar FROM daftar_lamaran WHERE id_lowongan='$resultLowongan[id_lowongan]'";
                        $prosesJumlahPelamar = mysqli_query($conn, $queryJumlahPelamar);
                        $resultJumlahPelamar = mysqli_fetch_assoc($prosesJumlahPelamar);

                ?>

                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="course-item">
                        <img src="assets/img/lowongan/<?php echo $resultLowongan['gambar_lowongan']; ?>" class="img-fluid" style="height: 250px;width: 100%;" alt="<?php echo $resultLowongan['nama_lowongan']; ?>">
                        <div class="course-content">
                            <div class="d-flex justify-content-between align-items-center mb-3">
                                <h4><?php echo $resultLowongan['nama_kategori_lowongan']; ?></h4>
                                <p class="price">Rp<?php echo rupiah($resultLowongan['gaji']); ?></p>
                            </div>

                            <h3><a href="index.php?content=detail-lowongan&id=<?php echo $resultLowongan['id_lowongan']; ?>"><?php echo $resultLowongan['nama_lowongan']; ?></a></h3>
                            <p><?php echo strip_tags(substr($resultLowongan['deskripsi'], 0, 100)); ?> ...</p>

                            <div class="trainer d-flex justify-content-between align-items-center">
                                <div class="trainer-profile d-flex align-items-center">
                                    <i class="icofont-building-alt"></i>
                                    <span><?php echo $resultLowongan['nama_perusahaan']; ?></span>
                                </div>
                                <div class="trainer-rank d-flex align-items-center">
                                    <i class="icofont-users-alt-5"></i>&nbsp;<?php echo $resultJumlahPelamar['jumlahPelamar']; ?></span>&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- End Course Item-->

                <?php } ?>

            </div>

        </div>
    </section><!-- End Popular Courses Section -->

</main><!-- End #main -->
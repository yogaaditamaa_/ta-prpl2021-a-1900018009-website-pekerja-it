<?php

	include 'functions/conn.php';

	if (isset($_POST['login_pelamar'])) {
		$username 	= $_POST['username'];
		$password 	= $_POST['password'];

		$queryLoginPelamar	= "SELECT * FROM pelamar WHERE username='$username' AND password='$password' ";
		$prosesLoginPelamar	= mysqli_query($conn, $queryLoginPelamar);
        $resultLoginPelamar    	= mysqli_fetch_assoc($prosesLoginPelamar);

        if (!empty($resultLoginPelamar)) {
            session_start();
            $_SESSION['id_pelamar']     = $resultLoginPelamar['id_pelamar'];
            $_SESSION['nama_pelamar']	= $resultLoginPelamar['nama_pelamar'];
            $_SESSION['jenis_kelamin']  = $resultLoginPelamar['jenis_kelamin'];
            $_SESSION['cv']  			= $resultLoginPelamar['cv'];
            $_SESSION['username']       = $resultLoginPelamar['username'];
            $_SESSION['password']       = $resultLoginPelamar['password'];

            header('location:index.php?content=akun-saya');
        }else{
        	echo "<script>window.alert('Username atau Password yang anda masukkan salah!'); window.location(history.back(-1))</script>";
        }
	}

	if (isset($_POST['login_perusahaan'])) {
		$username 	= $_POST['username'];
		$password 	= $_POST['password'];

		$queryLoginPerusahaan	= "SELECT * FROM perusahaan WHERE username='$username' AND password='$password' ";
		$prosesLoginPerusahaan	= mysqli_query($conn, $queryLoginPerusahaan);
        $resultLoginPerusahaan    	= mysqli_fetch_assoc($prosesLoginPerusahaan);

        if (!empty($resultLoginPerusahaan)) {
            session_start();
            $_SESSION['id_perusahaan']     	= $resultLoginPerusahaan['id_perusahaan'];
            $_SESSION['nama_perusahaan']	= $resultLoginPerusahaan['nama_perusahaan'];
            $_SESSION['lokasi_perusahaan']  = $resultLoginPerusahaan['lokasi_perusahaan'];
            $_SESSION['username']       	= $resultLoginPerusahaan['username'];
            $_SESSION['password']       	= $resultLoginPerusahaan['password'];

            header('location:perusahaan/index.php?content=dashboard');
        }else{
        	echo "<script>window.alert('Username atau Password yang anda masukkan salah!'); window.location(history.back(-1))</script>";
        }
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Login Website Pekerja IT</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://use.fontawesome.com/db20e36279.js"></script>
</head>
<body>

	<div class="container">
	    <div class="row py-5 mt-4 align-items-center">
	        <!-- For Demo Purpose -->
	        <div class="col-md-5 pr-lg-5 mb-5 mb-md-0 text-center">
	            <img src="https://res.cloudinary.com/mhmd/image/upload/v1569543678/form_d9sh6m.svg" alt="" class="img-fluid mb-3 d-none d-md-block">
	            <h1>Yuk Login Ke Akun Anda</h1>
	            <p class="text-muted mb-0">Silahkan login menggunakan Username & Password yang anda punya</p>
	        </div>

	        <!-- Registeration Form -->
	        <div class="col-md-7 col-lg-6 ml-auto border shadow py-4">

	        	<ul class="nav nav-tabs mb-4" role="tablist">
				    <li class="nav-item">
				      	<a class="nav-link font-weight-bold active" data-toggle="tab" href="#pelamar"><i class="fa fa-user"></i> Pelamar</a>
				    </li>
				    <li class="nav-item">
				      	<a class="nav-link font-weight-bold" data-toggle="tab" href="#perusahaan"><i class="fa fa-building"></i> Perusahaan</a>
				    </li>
				</ul>

				<div class="tab-content">

		            <form action="" method="POST" id="pelamar" class="tab-pane active">
		                <div class="row">

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="username" placeholder="Username" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-key text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="password" name="password" placeholder="Password" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <!-- Submit Button -->
		                    <div class="form-group col-lg-12 mx-auto">
		                        <button type="submit" name="login_pelamar" class="btn btn-success btn-block">
		                            <span class="font-weight-bold">LOGIN <i class="fa fa-sign-in"></i></span>
		                        </button>
		                    </div>
		                    <div class="col-lg-12 text-center">
		                    	<p>Belum punya akun?</p>
		                    </div>
		                    <div class="form-group col-lg-12 mx-auto">
		                        <a href="signup.php" class="btn btn-primary btn-block">
		                            <span class="font-weight-bold">DAFTAR <i class="fa fa-pencil-square-o"></i></span>
		                        </a>
		                    </div>

		                </div>
		            </form>

		            <form action="" method="POST" id="perusahaan" class="tab-pane fade">
		                <div class="row">

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="username" placeholder="Username" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-key text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="password" name="password" placeholder="Password" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <!-- Submit Button -->
		                    <div class="form-group col-lg-12 mx-auto">
		                        <button type="submit" name="login_perusahaan" class="btn btn-success btn-block">
		                            <span class="font-weight-bold">LOGIN <i class="fa fa-sign-in"></i></span>
		                        </button>
		                    </div>
		                    <div class="col-lg-12 text-center">
		                    	<p>Belum punya akun?</p>
		                    </div>
		                    <div class="form-group col-lg-12 mx-auto">
		                        <a href="signup.php" class="btn btn-primary btn-block">
		                            <span class="font-weight-bold">DAFTAR <i class="fa fa-pencil-square-o"></i></span>
		                        </a>
		                    </div>

		                </div>
		            </form>

		        </div>

	        </div>
	    </div>
	</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
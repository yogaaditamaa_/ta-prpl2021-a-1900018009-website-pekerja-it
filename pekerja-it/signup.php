<?php

	include 'functions/conn.php';
	include 'functions/karakter_unik.php';

	if (isset($_POST['daftar_pelamar'])) {
		$nama_pelamar 	= $_POST['nama_pelamar'];
		$jenis_kelamin 	= $_POST['jenis_kelamin'];
		$username 		= $_POST['username'];
		$password 		= $_POST['password'];

        // Include CV
        $cv 			= $_FILES['cv']['name']; // mendapatkan nama cv
        $lokasi_cv		= $_FILES['cv']['tmp_name']; // mendapatkan lokasi cv
		$tipe_file_cv   = format_gambar(strtolower($_FILES['cv']['type']));
        $tujuan_cv		= 'assets/img/cv'; // pindah cv tersebut ke lokasi ini
		$nama_cv_unik	= "cv-".karakter_unik($nama_pelamar)."-".$username.".".$tipe_file_cv;
        $upload_cv		= move_uploaded_file($lokasi_cv, $tujuan_cv.'/'.$nama_cv_unik); // function mengupload/memindahkan file ke direktori yang di maksud

		$queryDaftarPelamar		= "INSERT INTO pelamar VALUES ('', '$nama_pelamar', '$jenis_kelamin', '$nama_cv_unik', '$username', '$password')";
        $prosesDaftarPelamar	= mysqli_query($conn, $queryDaftarPelamar);

        if (!empty($prosesDaftarPelamar)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'login.php';</script>";
        }else{
        	echo "<script>window.alert('Gagal, ulangi!'); window.location(history.back(-1))</script>";
        }
	}

	if (isset($_POST['daftar_perusahaan'])) {
		$nama_perusahaan 	= $_POST['nama_perusahaan'];
		$lokasi_perusahaan 	= $_POST['lokasi_perusahaan'];
		$username 			= $_POST['username'];
		$password 			= $_POST['password'];

		$queryDaftarPerusahaan	= "INSERT INTO perusahaan VALUES ('', '$nama_perusahaan', '$lokasi_perusahaan', '$username', '$password')";
        $prosesDaftarPerusahaan	= mysqli_query($conn, $queryDaftarPerusahaan);

        if (!empty($prosesDaftarPerusahaan)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'login.php';</script>";
        }else{
        	echo "<script>window.alert('Gagal, ulangi!'); window.location(history.back(-1))</script>";
        }
	}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Daftar Website Pekerja IT</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://use.fontawesome.com/db20e36279.js"></script>
</head>
<body>

	<div class="container">
	    <div class="row py-5 mt-4 align-items-center">
	        <!-- For Demo Purpose -->
	        <div class="col-md-5 pr-lg-5 mb-5 mb-md-0 text-center">
	            <img src="https://res.cloudinary.com/mhmd/image/upload/v1569543678/form_d9sh6m.svg" alt="" class="img-fluid mb-3 d-none d-md-block">
	            <h1>Yuk Daftarkan Diri Anda</h1>
	            <p class="text-muted mb-0">Silahkan isi formulir di samping dengan lengkap & benar!</p>
	        </div>

	        <!-- Registeration Form -->
	        <div class="col-md-7 col-lg-6 ml-auto border shadow py-4">

	        	<ul class="nav nav-tabs mb-4" role="tablist">
				    <li class="nav-item">
				      	<a class="nav-link font-weight-bold active" data-toggle="tab" href="#pelamar"><i class="fa fa-user"></i> Pelamar</a>
				    </li>
				    <li class="nav-item">
				      	<a class="nav-link font-weight-bold" data-toggle="tab" href="#perusahaan"><i class="fa fa-building"></i> Perusahaan</a>
				    </li>
				</ul>

				<div class="tab-content">

		            <form action="" method="POST" enctype="multipart/form-data" id="pelamar" class="tab-pane active">
		                <div class="row">

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="nama_pelamar" placeholder="Nama Pelamar" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
			                    <select class="custom-select" name="jenis_kelamin">
								  	<option value="Laki-Laki">Laki-Laki</option>
								  	<option value="Perempuan">Perempuan</option>
								</select>
							</div>

		                    <div class="input-group col-lg-12">
			                    <div class="input-group mb-3">
								  	<div class="input-group-prepend">
								    	<span class="input-group-text"><i class="fa fa-file-text-o"></i></span>
								  	</div>
								  	<div class="custom-file">
								    	<input type="file" class="custom-file-input" id="inputGroupFile01" name="cv" required>
								    	<label class="custom-file-label" for="inputGroupFile01">Pilih CV anda</label>
								  	</div>
								</div>
							</div>

		                    <div class="input-group col-lg-12 border-top mb-4 mt-1">
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="username" placeholder="Username" autocomplete="off" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-key text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="password" name="password" placeholder="Password" autocomplete="off" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <!-- Submit Button -->
		                    <div class="form-group col-lg-12 mx-auto">
		                        <button type="submit" name="daftar_pelamar" class="btn btn-success btn-block">
		                            <span class="font-weight-bold">DAFTAR <i class="fa fa-pencil-square-o"></i></span>
		                        </button>
		                    </div>
		                    <div class="col-lg-12 text-center">
		                    	<p>Sudah punya akun?</p>
		                    </div>
		                    <div class="form-group col-lg-12 mx-auto">
		                        <a href="login.php" class="btn btn-primary btn-block">
		                            <span class="font-weight-bold">LOGIN <i class="fa fa-sign-in"></i></span>
		                        </a>
		                    </div>

		                </div>
		            </form>

		            <form action="" method="POST" id="perusahaan" class="tab-pane fade">
		                <div class="row">

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user-circle text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="nama_perusahaan" placeholder="Nama Perusahaan" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-address-book-o text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="lokasi_perusahaan" placeholder="Lokasi Perusahaan" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 border-top mb-4 mt-1">
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-user text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="text" name="username" placeholder="Username" autocomplete="off" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <div class="input-group col-lg-12 mb-4">
		                        <div class="input-group-prepend">
		                            <span class="input-group-text bg-white px-4 border-md border-right-0">
		                                <i class="fa fa-key text-muted"></i>
		                            </span>
		                        </div>
		                        <input type="password" name="password" placeholder="Password" autocomplete="off" class="form-control bg-white border-left-0 border-md" required>
		                    </div>

		                    <!-- Submit Button -->
		                    <div class="form-group col-lg-12 mx-auto">
		                        <button type="submit" name="daftar_perusahaan" class="btn btn-success btn-block">
		                            <span class="font-weight-bold">DAFTAR <i class="fa fa-pencil-square-o"></i></span>
		                        </button>
		                    </div>
		                    <div class="col-lg-12 text-center">
		                    	<p>Sudah punya akun?</p>
		                    </div>
		                    <div class="form-group col-lg-12 mx-auto">
		                        <a href="login.php" class="btn btn-primary btn-block">
		                            <span class="font-weight-bold">LOGIN <i class="fa fa-sign-in"></i></span>
		                        </a>
		                    </div>

		                </div>
		            </form>

		        </div>

	        </div>
	    </div>
	</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>
<?php

    session_start();

    include 'functions/conn.php';
    include 'functions/rupiah.php';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Website Pekerja IT</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

    <!-- ======= Header ======= -->
    <?php require 'content/header.php'; ?>
    <!-- End Header -->

    <?php
        if ($_GET['content']=="home") {
            require 'content/home.php';
        }elseif ($_GET['content']=="lowongan") {
            require 'content/lowongan.php';
        }elseif ($_GET['content']=="detail-lowongan") {
            require 'content/detail-lowongan.php';
        }elseif ($_GET['content']=="akun-saya") {
            require 'content/akun-saya.php';
        }else{
            header('location: index.php?content=home');
        }
    ?>

    <!-- ======= Footer ======= -->
    <?php require 'content/footer.php'; ?>
    <!-- End Footer -->

    <!-- Vendor JS Files -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>
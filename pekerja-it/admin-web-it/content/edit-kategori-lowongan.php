<?php

    date_default_timezone_set("Asia/Jakarta");

    $queryEdit  = "SELECT * FROM kategori_lowongan WHERE id_kategori_lowongan='$_GET[id]'";
    $prosesEdit = mysqli_query($conn, $queryEdit);
    $resultEdit = mysqli_fetch_assoc($prosesEdit);

    if (isset($_POST['edit'])) {
        $nama_kategori_lowongan     = $_POST['nama_kategori_lowongan'];

        $queryEditKategoriLowongan  = "UPDATE kategori_lowongan SET nama_kategori_lowongan='$nama_kategori_lowongan' WHERE id_kategori_lowongan='$_GET[id]'";
        $prosesEditKategoriLowongan = mysqli_query($conn, $queryEditKategoriLowongan);

        if (!empty($prosesEditKategoriLowongan)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=kategori-lowongan';</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-edit"></i> Edit Kategori Lowongan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" class="col-12" enctype="multipart/form-data">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-primary mb-4">Silahkan isi data di bawah ini dengan lengkap & benar!</h1>
                        </div>

                        <div class="form-group">
                            <label for="nama_kategori_lowongan">Nama Kategori Lowongan</label>
                            <input type="text" id="nama_kategori_lowongan" class="form-control form-control-user" name="nama_kategori_lowongan" value="<?php echo $resultEdit['nama_kategori_lowongan']; ?>" required>
                        </div>
                        <button type="submit" name="edit" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
<?php

    date_default_timezone_set("Asia/Jakarta");

    if (isset($_POST['selesai'])) {
        $nama_kategori_lowongan = $_POST['nama_kategori_lowongan'];

        $queryKategoriLowongan  = "INSERT INTO kategori_lowongan VALUES ('', '$nama_kategori_lowongan')";
        $prosesKategoriLowongan = mysqli_query($conn, $queryKategoriLowongan);

        if (!empty($prosesKategoriLowongan)) {
            echo "<script>window.alert('Berhasil!'); location.href = 'index.php?content=kategori-lowongan';</script>";
        }
    }

?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-edit"></i> Buat Kategori Lowongan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12">
            <div class="card border-left-primary shadow h-100 py-2">
                <form action="" method="POST" class="col-12">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-primary mb-4">Silahkan isi data di bawah ini dengan lengkap & benar!</h1>
                        </div>

                        <div class="form-group">
                            <label for="nama_kategori_lowongan">Nama Kategori Lowongan</label>
                            <input type="text" id="nama_kategori_lowongan" class="form-control form-control-user" name="nama_kategori_lowongan" placeholder="Masukkan Nama Lowongan" required>
                        </div>
                        <button type="submit" name="selesai" class="btn btn-primary btn-user btn-block">SELESAI <i class="fa fa-check"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-building"></i> Daftar Perusahaan</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 5%">No</th>
                                <th>Nama Perusahaan</th>
                                <th>Lokasi Perusahaan</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryPerusahaan  = "SELECT * FROM perusahaan ORDER BY id_perusahaan DESC";
                                $prosesPerusahaan = mysqli_query($conn, $queryPerusahaan);
                                while ($resultPerusahaan   = mysqli_fetch_assoc($prosesPerusahaan)) {

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $resultPerusahaan['nama_perusahaan']; ?></td>
                                <td><?= $resultPerusahaan['lokasi_perusahaan']; ?></td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-pray"></i> Daftar Pelamar</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- DataTales Example -->
        <div class="col-12 card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width: 5%;">No</th>
                                <th>Nama Pelamar</th>
                                <th>Jenis Kelamin</th>
                                <th>Cek CV</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php

                                $no = 1;
                                $queryDaftarPelamar  = "SELECT * FROM pelamar ORDER BY id_pelamar DESC";
                                $prosesDaftarPelamar = mysqli_query($conn, $queryDaftarPelamar);
                                while ($resultDaftarPelamar   = mysqli_fetch_assoc($prosesDaftarPelamar)) {

                            ?>

                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $resultDaftarPelamar['nama_pelamar']; ?></td>
                                <td><?= $resultDaftarPelamar['jenis_kelamin']; ?></td>
                                <td class="text-center">
                                    <a target="_blank" href="../assets/img/cv/<?php echo $resultDaftarPelamar['cv']; ?>" class="btn btn-warning">
                                        <i class="fas fa-external-link-alt"></i> CV
                                    </a>
                                </td>
                            </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
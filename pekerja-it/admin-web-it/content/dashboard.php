<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard Admin Web Pekerja IT</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <?php

            $queryFeeRoyalty        = "SELECT SUM(fee_royalty_perusahaan) AS feeRoyalty FROM fee_royalty";
            $prosesFeeRoyalty       = mysqli_query($conn, $queryFeeRoyalty);
            $resultFeeRoyalty       = mysqli_fetch_assoc($prosesFeeRoyalty);

            $queryPerusahaan        = "SELECT COUNT(id_perusahaan) AS perusahaanTerdaftar FROM perusahaan";
            $prosesPerusahaan       = mysqli_query($conn, $queryPerusahaan);
            $resultPerusahaan       = mysqli_fetch_assoc($prosesPerusahaan);

            $queryLowonganKerjaan   = "SELECT COUNT(id_lowongan) AS lowonganKerjaan FROM lowongan";
            $prosesLowonganKerjaan  = mysqli_query($conn, $queryLowonganKerjaan);
            $resultLowonganKerjaan  = mysqli_fetch_assoc($prosesLowonganKerjaan);

            $queryPelamar           = "SELECT COUNT(id_pelamar) AS pelamar FROM pelamar";
            $prosesPelamar          = mysqli_query($conn, $queryPelamar);
            $resultPelamar          = mysqli_fetch_assoc($prosesPelamar);

        ?>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Jumlah Fee Royalty</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Rp<?= rupiah($resultFeeRoyalty['feeRoyalty']); ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="far fa-money-bill-alt fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-12 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Perusahaan Terdaftar</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rupiah($resultPerusahaan['perusahaanTerdaftar']); ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-building fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Jumlah Lowonan Kerjaan</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rupiah($resultLowonganKerjaan['lowonganKerjaan']); ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-briefcase fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Pelamar</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><?= rupiah($resultPelamar['pelamar']); ?></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-pray fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>